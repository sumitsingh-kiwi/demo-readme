# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
# from django.views.decorators.csrf import
from django.http import HttpResponse
from rest_framework.response import Response
#from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework import serializers, viewsets
#from rest_framework.permissions import AllowAny
#from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.decorators import detail_route
# from rest_framework.reverse import reverse
# import vimeo
# from rest_framework import viewsets
#from rest_framework import viewsets
#from rest_framework import
from newApp.models import Person, BasicInfo, Note, Tag



# Create your views here.


class ClassSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = ('name', 'doc')


class BasicSerializer(serializers.ModelSerializer):
    person= ClassSerializer(read_only=False)
    class Meta:
        model = BasicInfo
        fields = '__all__'


class FooField(serializers.RelatedField):

    def to_representation(self, obj):
        # print(dir(obj))
        return obj.name

    def to_internal_value(self, data):
        try:
            # obj_id = data
            # exist_qs = Tag.objects.filter( name__in=obj_id )
            # list1 = list( set( data ) - set( [obj['name'] for obj in exist_qs.values( 'name' )] ) )
            # new_qs = Tag.objects.bulk_create( [Tag( name=obj ) for obj in list1] )

            return data
        except Exception:
            # print(e)
            raise serializers.ValidationError(
                'id must be an integer.'
            )


class NoteSerializer(serializers.ModelSerializer):
    # tags= TagSerializer(many= True)
    tags = FooField(
        queryset=Tag.objects.all()
    )

    def create(self, validated_data):
        tag = validated_data['tags']
        validated_data.pop('tags')
        tag_obj = Tag.objects.filter(name__in=tag)
        instance = Note.objects.create(**validated_data)
        instance.tags.add(*list(tag_obj))
        instance.save()
        return instance

    class Meta:
        model = Note
        fields = ('title', 'description', 'tags')


def app_login(request):
    print 'inside login url'
    return render(request, 'login.html', {
        'foo': 'bar',
    })


@csrf_exempt
def loginView(request):
    # print "data-->", request
    return HttpResponse("hi")


def Sidebar(request):
    # print "data-->", request
    return render(request, 'sidebar.html', {})


class ClassBasedView(GenericAPIView):
    serializer_class = ClassSerializer
    parser_classes = (FormParser,)
    """
	Return List of all existing urls
	"""

    def get(self, request):
        """
		Get particular user
		"""
        return HttpResponse("Hi")

    def post(self, request):
        """	Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        Your docs """
        # name = request.data['name']
        return HttpResponse("created")


# @method_decorator(csrf_protect)
# def app_logout(request):
#     return HttpResponse()


class ViewSetDemo(viewsets.ModelViewSet):
    model = Person
    queryset = Person.objects.all()
    serializer_class = ClassSerializer
    parser_classes = (JSONParser, MultiPartParser, FormParser)

    def create(self, request):
        """
		- name: age
		  type: integer
		"""
        data = request.data
        name = data['name']
        # demoField = data['demoField']
        Person.objects.create(name=name)
        return Response("From Viewset")


class RouterDemo(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = ClassSerializer
    lookup_field = 'name'

    @detail_route(methods=['put'], url_path='password')
    def customPostMethod(self, request, name):
        qs = self.get_object()
        print("name--->", name, qs)
        qs.name = request.data['name']
        qs.save()
        return Response("Done")


class NameserverViewSet(viewsets.ModelViewSet):
    queryset = BasicInfo.objects.all()
    serializer_class = BasicSerializer

    # lookup_field= 'id'
    def get_queryset(self):
        # print(self.kwargs)
        # print("reverse url-->", reverse('newApp:nested-list', kwargs={'person_name': 'ak'}))
        return BasicInfo.objects.filter(person__name=self.kwargs['person_name'])


# lookup_field= 'name'


class NoteViewset(viewsets.ModelViewSet):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = '__all__'
