# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from newApp.models import Person, BasicInfo, Note, Tag, Product, LikeActivity
# Register your models here.
admin.site.register(Person)
admin.site.register(BasicInfo)
admin.site.register(Tag)
admin.site.register(Note)
admin.site.register(Product)
admin.site.register(LikeActivity)
