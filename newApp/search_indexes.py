import datetime
from haystack import indexes
from newApp.models import Tag


class NoteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True, template_name="/home/kiwitech/sumit/sessionDemo/templates/person/person_text.txt")
    name = indexes.CharField(model_attr="name", null=True)
    content_auto = indexes.EdgeNgramField(model_attr='name')

    def get_model(self):
        return Tag

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()