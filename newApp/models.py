# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


# from django_cache_manager.cache_manager import CacheManager

# Create your models here.
class Person( models.Model ):
    name = models.CharField( max_length=100, null=True, blank=True )
    doc = models.FileField( blank=True, null=True )

    def __str__(self):
        return self.name


class BasicInfo( models.Model ):
    person = models.ForeignKey( Person, blank=True, null=True, related_name='single' )
    mul = models.ManyToManyField( Person, blank=True, related_name='multiple' )
    age = models.PositiveIntegerField( default=21 )
    salary = models.PositiveIntegerField( default=20000 )

    def __str__(self):
        return self.person.name


class Tag( models.Model ):
    name = models.CharField( max_length=100, primary_key=True )

    def __str__(self):
        return self.name


class Note( models.Model ):
    title = models.CharField( max_length=100, null=True, blank=True )
    description = models.CharField( max_length=200, null=True, blank=True )
    tags = models.ManyToManyField( Tag, blank=True )

    # objects = CacheManager()

    def __str__(self):
        return self.title


class Product( models.Model ):
    prod_id = models.CharField( max_length=20, blank=True, null=True )
    prod_name = models.CharField( max_length=100, blank=True, null=True )
    quantity = models.PositiveIntegerField( default=0 )

    def __str__(self):
        return self.prod_id


class LikeActivity(models.Model):
    name = models.CharField(max_length=20, blank=True, null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_objects = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.name
