from django.conf.urls import url, include
# from django.contrib.auth import views as auth_views
from rest_framework_nested import routers
from rest_framework_jwt.views import refresh_jwt_token
from newApp import views
from newApp import views2
# from rest_framework.routers import DefaultRouter

# router = DefaultRouter(trailing_slash=True)
# router.register(r'viewset', views.ViewSetDemo, base_name= 'viewset')
# router.register(r'route', views.RouterDemo, base_name= 'RouterDemo')

router2 = routers.SimpleRouter()
router2.register(r'excel', views2.ExcelView, base_name='excel')

router = routers.SimpleRouter()
router.register(r'viewset', views.ViewSetDemo)
router.register(r'route', views.RouterDemo)
router.register(r'note', views.NoteViewset)

domains_router = routers.NestedSimpleRouter(router, r'route', lookup='person')
domains_router.register(r'nested', views.NameserverViewSet, base_name='nested')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(router2.urls)),
    url(r'^', include(domains_router.urls)),
    url(r'^login/$', views.app_login, name='login'),
    url(r'^loginView/$', views.loginView, name='loginView'),
    url(r'^sidebar/$', views.Sidebar, name='Sidebar'),
    # url(r'^logout/$', views.app_logout, name='logout'),
    url(r'^class/$', views.ClassBasedView.as_view(), name='ClassBasedView'),
    url(r'^search/', views2.tagSearch),
    url(r'^getToken/$', views2.get_token),
    url( r'^checkToken/$', views2.check_token),
    url(r'^api-token-refresh/', refresh_jwt_token, name='refresh-token'),
]
