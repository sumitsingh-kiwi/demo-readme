from rest_framework import serializers


class CommentSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    text = serializers.CharField(max_length=200)