# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from django.http import HttpResponse
import json
from rest_framework.response import Response
from rest_framework import viewsets

# from newApp.views import NoteSerializer
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
# import xlrd, csv, os
import datetime
from django.utils.decorators import method_decorator
from haystack.query import SQ
from custom_middleware.utils import get_cache_key
from django.core.cache import cache

# from django.conf import settings
# from django.core.cache.backends.base import DEFAULT_TIMEOUT
from custom_middleware.cache_decorator import cache_page
from newApp.models import Note, Product, Tag
from newApp.serializers import CommentSerializer
# CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


class ExcelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class CreateExcelSerializer(serializers.ModelSerializer):
    upload = serializers.FileField()

    class Meta:
        model = Product
        fields = ('upload',)

    # def create(self, validated_data):
    # 	p = validated_data['upload']  # .read()
    # 	wb = xlrd.open_workbook(filename=None, file_contents=p.read())
    # 	sheet = wb.sheet_by_index(0)
    # 	print(sheet.row_values(0), sheet.nrows, sheet.ncols)
    # 	for row in range(1, sheet.nrows):
    # 		for col in range(sheet.ncols):
    # 			print(col)
    #
    # 	return None
from rest_framework.authentication import BasicAuthentication

# @cache_page(CACHE_TTL) CacheResponseAndETAGMixin,
class ExcelView(viewsets.ModelViewSet ):
    queryset = Product.objects.all()
    # authentication_classes = [BasicAuthentication]

    def get_serializer_class(self):
        if self.request.method == "POST":
            return CreateExcelSerializer
        else:
            return ExcelSerializer

    # except:
    # 	return ExcelSerializer
    # @method_decorator( cache_page( 60 ) )
    def dispatch(self, *args, **kwargs):
        return super( ExcelView, self ).dispatch( *args, **kwargs )

    def list(self, request):
        t1 = datetime.datetime.now()
        qs = Note.objects.all()
        list(qs)
        name = qs.first().title
        id = qs.first().id
        t2 = datetime.datetime.now()
        print("time taken is--->", (t2 - t1).seconds, t1, t2)
        return Response("time taken is--->" + str((t2 - t1).seconds) + name + str(id))

    def create(self, request):
        key = get_cache_key(request)
        cache.delete( key )
        return Response("ok")


from haystack.query import SearchQuerySet
from django.http import HttpResponse

class TagSerializer(serializers.Serializer):
    class Meta:
        model = Tag
        fields = '__all__'


def tagSearch(request):
    """My custom search view."""
    q = request.GET['q']
    # sqs = SearchQuerySet().filter(name__startswith=q)[:5]
    sqs = SearchQuerySet().autocomplete(content_auto=q)
    # sqs = SearchQuerySet().autocomplete(content_auto=q)
    # sqs = Tag.objects.filter(name__icontains=q)
    data = CommentSerializer(sqs, many=True)
    # sqs = [obj.name for obj in sqs]
    # print(sqs)
    return HttpResponse(json.dumps(data.data))


def get_token(request):

    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    # payload = jwt_payload_handler(request.user)
    payload = {'a': 1}#, 'time': datetime.datetime.now().strftime('%H %M %S')}
    token = jwt_encode_handler(payload)
    # token = "safdsfdsfds"
    return HttpResponse(token)

# from rest_framework.test import APIClient
# client = APIClient()
from django.urls import reverse
def check_token(request):
    # result = {'a':1}
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhIjoxfQ.XqlecO-LF1i-PUHyCRydAoFrWSX4XeRzefgJ4zwBV2w'
    print(request.user)
    # view = refresh_jwt_token(request)
    # url = reverse('refresh-token')
    #
    # res = client.post(url, {"token":token})
    # print(client.response)
    try:

        payload = api_settings.JWT_DECODE_HANDLER(token)
        print(payload)
    # except jwt.ExpiredSignature:
    #     msg = _( 'Signature has expired.' )
    #     raise serializers.ValidationError( msg )
    # except jwt.DecodeError:
    #     msg = _( 'Error decoding signature.' )
    #     raise serializers.ValidationError( msg )
    except Exception as e:
        print(e)
        return HttpResponse("error")
    # return payload

    return HttpResponse({'q':1})