
# Quadrant VMS Api  
  
### Setup steps:  

1. Environment setup  
   
    * create virtual env:  
         * Recomended pytohn verison 3.5.2  
    * install requirements using command:  
         * pip install requirements.txt  
  
2. Database Setup

     * setup database server:  
         * sudo apt-get install postgresql postgresql-contrib  
         * sudo apt-get install postgresql-server  
  
     * create database with proper permissions to the db-user:  
         * create user user_name;  
         * \password user_name;  
         * create database db_name;  
         * grant all privileges on database db_name to user_name;  
  
     * add db_name, user_name, user_password in your env file  
         so that we can use os.environ[] settings to operate on them  
  
     * DB migrations:  
         * python manage.py makemigrations  
         * python manage.py migrate  
  
     * load fixtures:  
         * python manage.py loaddata fixture/initial.json  
  
3. Run servers:

     * celery -A quadrant worker -l info  
     * python manage.py runserver